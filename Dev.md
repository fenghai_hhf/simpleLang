# CodeGen
CodeGen 用来生成 llvm 代码，CodeGen::type 是 llvm 类型，但和生成的 llvm::Value 有区别，type 可以能是整数、浮点、结构，但返回 Value 固定为指针。

# 对象的数据结构

| 占用字节数 | 说明                                                         | 相关控制位 |
| ---------- | ------------------------------------------------------------ | ---------- |
| 4字节      | 数组长度                                                     | 0          |
| 4字节      | 对象大小                                                     | 0+7        |
| 4字节      | 数组索引位                                                   | 3          |
| 1字节      | 标志位                                                       |            |
| 7字节      | 类型ID，非默认类型ID的计算方式一般为类全名（包含包名）的 md5-16值去除首位 | 7          |
| 不定       | 对象数据                                                     |            |

指针会指向对象数据，其他数据通过指针向上回退获得

## 标志定义

0~7 位：

	0	是否数组
	1	是否具有引用计数（没引用计数的话，需要手工释放）
	3	是否指向数组的指针
	4	是否有符号(保留)
	5	
	6	
	7	是否默认类型(如果是否表示 class)

如果是默认类型，类型为:
	

| 序号 | 类型 |
|:---|:-----|
| 0 | Func 函数指针 |
| 1 | boolean |
| 2 | byte	    |
| 3 | char	    |
| 4 | short		|
| 5 | int		|
| 6 | long		|
| 7 | float		|
| 8 | double |
| 9 | interface |
| 10 | Any |
| 11 | Delay |
| 12 | Coroutine |
| 13 | Thread |
| 14 |  |

* 对象数据

# 对 c 的兼容

在语法层面去除了下划线在函数、变量中的使用，这样它就可以用来当分割符了，以便 c 函数导入。

模块分两种，Si 模块——以源码的形式导入，c 模块——直接通过 clang 来解析头文件，通过 export.h + dll 的方式导入。
如果没有模板，Si 模块也可以编译为 c 模块。

语法层面规定类必须大写开头，函数（成员函数）、变量必须以小写开头，既保证了风格统一，也帮助语法分析器可以分析 < M > 是否是模板，并且 C 语言中也可以使用 _ 和大小写搞定映射。

llvm ir 中的 struct 被映射为类，而静态函数被映射为 {类全名} _ S_ 函数 _ 后缀。
由于 ir 中 struct 的成员不存在名称，
因此成员变量的读写全部映射为 {类全名} _ {SET/GET} _ {index} _ {变量名}。
这样通过 llvm struct 和对应的 SET，GET 函数，就能完全映射类。

静态变量被映射为全局变量，_{类全名} _ {name}。

# 类继承

class MyClass : Base

在 llvm IR 中， MyClass 作为 Base 的扩展类型（前几个字段是 Base）的类型，Interface 作为模板约束，在 llvm IR 中被抛弃，由于不需要虚表什么的，可以直接使用指针把 MyClass 赋值给 Base.
如果需要 dycast, 那么需要读取头字节中的类型，然后判断是否可以转换（运行期）。

编译的时候，需要保存一份继承表，用来判断是否可以赋值和动态转换。

# 字符串

原生字符串为 byte[] 类型，默认 utf-8 编码，可以被赋值给 String 类，转换为 Unicode 编码，或者赋值给 Array<byte>。

# 类

源码级别的类使用 AstClass 保持，实例化以后，确定了模板类参数，使用 ClassInstance 保存。
ClassInstanceType 在 new 一个 AstClass 的时候生成，因为这个时候可以确定模板参数，然后用指定类型构造 llvm Type。
ClassInstanceType 内的成员函数，有可能还是模板的，因此保存为 AstFunction, 在函数被调用的时候才实例化。

包导入的时候，定义一般为源码级导入，具体 c 函数，被包装为 FunctionInstance 后，再反包装为 AstFunction 放回。

函数调用的时候，使用 名字 + 类型 的方式查找 c 函数，如果找到，优先调用 c 函数，如果找不到，尝试使用源码。                              
							  
# 上下文 Context

上下文有几种情况
# 上下文
## 全局
最外层的上下文环境

## 全局函数

在全局环境中，注入函数参数
函数内的上下文为函数自身 + 定义函数时的上下文（一般应该是全局）

## 类内
在类定义环境中，注入 this, 这个 this 由函数首个参数传入
类内上下文是类自身上下文 + 定义函数时的上下文（一般应该是全局）

    在全局环境中，注入类内静态变量，另外，考虑到下面的写法
    class My{
    	func in()
    	var x=in() 
    }
    还要注入方法。

## 成员函数上下文
成员函数上下文为成员函数自身 + 类上下文

## 类实例 ( obj.val 这种 )
	在类定义环境中，额外注入 this，这个 this 为对象实例

# 逃逸分析
## 区域变量逃逸分析
SiLang 需要在编译期进行逃逸分析以优化性能。  
我们以函数为单位进行逃逸分析，首先分析函数内变量是否会被返回，如果会，标记内部变量为“会逃逸”

	func myFunc( ) {
		MyCls a()		// a 创建为引用计数1		
		return a		// a 会逃逸
	}

当变量被整体赋值（引用）时标记为会逃逸

	func myFunc( Cls x ) {
		MyCls a()
		x.a = a  		// a 会逃逸
	}

变量被用于线程内，被标记为会逃逸

	func myFunc(){
		MyCls a()
		go {
			a	// 逃逸
		}
	}

## 参数逃逸分析
除了返回外，其他等同区域变量逃逸分析
非逃逸参数，标记为 const 的

	func MyFunc( MyCls a ){	// a 被标记为 const (不会逃逸)
		.. 
	}

## 组合
当变量被作为参数传入一个函数，如果这个函数表示参数是 const 的，那么不改变这个变量的逃逸性，否则标记它为会逃逸。

## 构造
非逃逸变量使用堆构造 (llvm 的 alloc)。逃逸变量默认使用 createObject 构造为标准对象，使用 GC 管理，未来如果类被声明为手工管理，对象使用 malloc 构造。

## 析构&内存释放
部分 **块（block)** 需要附带一个独立的释放块，并包含独立的释放堆栈，这包括函数块，分支块，协程块。这是由于执行路径不一致，释放栈是不一样的。

逃逸对象构造的时候引用计数1，并在放入释放堆栈，在当前块结束的时候，跳转到对应的释放块，调用 release。

逃逸变量在作为会逃逸参数被调用时，引用计数+1，被协程块引用时引用计数+1，被作为成员函数引用时引用计数+1，其他时候不需要。

几个特殊情况的思考：

new 了以后没被任何参数引用的对象肯定是非逃逸的，可以不用考虑。

考虑到会逃逸的对象 A

	var b = A
	var c = b
	go{ c }

这种情况下，变量 b 实际并不会逃逸，逃逸分析应该可以优化。但考虑到我们语言的定义，c 其实是 b 的别名，是完全一样的，所以应该不会带来多次引用计数加减。

# 数组

对象数组内部保存的是对象的指针

# 包管理
包使用固定的目录格式，编译完毕以后可以通过目录或者 zip/7z 的形式来导入

包的目录格式：

注意：除了首层目录，可以用 . 来把几层目录缩减到一层，以简化目录

	[package name]-0.0.1-release ->  包目录(包含版本号)，或者 zip 文件名(zip 改为 spz 后缀名, 7z 改为 sp7 后缀), release 可以省略
	  └ src							->  源码目录，sl 文件放在这里，所有的文件都会被导入
		   └ [*.si]					-> src 内部仍然通过目录结构来存放源文件，并且包名会作为前缀
	   └ overload				-> 重载目录
	           └ -> [other package(org.si)]  -> 可以注入其他包
		└ resources				-> 资源目录
		└ platform				-> 各平台的库
			└ x86_64-pc-windows-msvc		-> 参见 llvm Triple 
					└ share					-> 动态库（dll 或者 so 文件) 及其对应的 lib 文件
					└ static				-> 静态库 (lib 文件)
		└ include							-> .h 头文件(如果有的话)
		└ export.h			-> 导出头文件，编译器会解析这个文件来导入其他文件，如果没有，说明是纯源码库
		└ meta.txt			-> 包描述文件      

meta.txt 文件

	version: 1.0.1							-> 3层数字版本号 + 字符，                             前两位表示接口稳定，有接口删除、参数改变的情况必须升级第一位
											-> 第二位允许接口有增加，但其他接口必须稳定
											-> 第三位表示 bug 修正，接口稳定
	configure: release 					-> 如果是 release, 可以省略 
	dependency: org.other.package-1.3.0	-> 引用包名，每个引用一行，版本号是最低版本，编译器会尝试使用最新稳定接口版本（第一位相同

​                                                               

# 接口

接口在代码里是同一种写法，但编译器会用不同的方式来实现接口。甚至某些非接口也会转为接口来实现。

## 源码函数的参数

带源码的函数的参数，如果附带有接口，那么它会当成模板函数被展开。

```
interface MyInterface
func myFunc( MyInterface inf ){		// 这个函数会被当初模板函数处理
	...
}
```

顺便，对于普通含源码函数，所有参数类也当作接口来处理，这样就可以实现 golang 的函数调用逻辑

```
MyClass my;
func youFunc( YouClass you ){}	// 函数带源码的情况下 YouClass 被视作接口
youFunc(my)	// 这个调用会当模板函数调用展开
```

## 赋值接口、非源码函数的参数

```
MyInterface inf=myClass
func myFunc( MyInterface inf )	// 无源码
```

这种情况下，接口会被创建为一个结构 `struct` <interface_Name>，结构里存放一个 this 指针，所有的成员变量都是 getter, setter 的函数指针，成员函数都是函数指针。这些指针在接口赋值的时候，通过对应的类来创建并赋值。

## 类定义里的接口

接口在类定义里的时候，仅仅被当成定义约束，仅仅在编译期起作用，以保证所有必要的成员函数都被实现了。

## 接口继承

多个接口依次继承的情况下，所有的成员变量、函数都会被整合到最终接口里

```
interface Base {
	int bValue
}

interface MyIntf : Base {
	int mValue;
}
```
等同于

```
interface MyIntf {
	int bValue
	int mValue
}
```





# 继承的实现

```
class Base {
	int bValue 
		getter {
			return bValue
		}
}

class MyClass : Base {
	int mValue
	override int bValue getter {
		return mValue
	}
}


```

# RTTI 运行时类型信息

 ```C++
class ClassType {
    const uint64_t id;
	string packageName;
    string className;
    map<string, Field> fields;
}
 ```

